import sys
sys.path.insert(0,'..')
import os
import logging
logging.getLogger('tensorflow').disabled = True
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from GAN import DCGAN, CDCGAN, ACDCGAN, logger
logger.setLevel(logging.ERROR)
import tensorflow as tf
import numpy as np
from sklearn.metrics import confusion_matrix, roc_auc_score
import datetime
from time import time, sleep
import itertools
import collections
import multiprocessing





# Checkpoint and log paths
ckpt_root   = "../checkpoints/models_GS/" # HERE
date        = datetime.datetime.now().strftime("%m%d-%H%M%S")
log_file    = ckpt_root + 'logfile_'+date+'.txt'

# Train and test data
dataset     = "../data/DI/local_norm/"
test_images = np.load(dataset + 'test_data_F160W.npy')
test_labels = np.ones(len(test_images), dtype=np.int)
train_data  = {
               'DCGAN':   {'data':   dataset+'train_data_F160W.npy'},
               'CDCGAN':  {'data':   dataset+'train_data.npy',
                           'labels': dataset+'train_labels.npy'},
               'ACDCGAN': {'data':   dataset+'train_data.npy',
                           'labels': dataset+'train_labels.npy'}
              }

# Generated images (F160W only)
n_images    = 1000
new_labels  = np.ones(n_images, dtype=np.int)

N_repeat    = 4 # HERE

# Grid Values (MODIFY HERE)
grid_values = collections.OrderedDict({ # HERE
                                       'model':  ['DCGAN', 'CDCGAN', 'ACDCGAN'],
                                       'bs':     [2, 4, 8, 16, 32],
                                       'lr':     [1e-03, 5e-04, 2e-04],
                                       'zdim':   [10, 100],
                                       'gsteps': [1, 2],
                                       })


def run_hyperparams(list_values):
    p_id   = int(multiprocessing.current_process()._identity[0]) - 1
    dev_id = p_id % NUM_GPU
    os.environ['CUDA_VISIBLE_DEVICES'] = "%i" % dev_id
    
    str_values = '_'.join([str(v) for v in list_values])
    
    date   = datetime.datetime.now().strftime("%m%d-%H%M%S")
    ckpt_d = ckpt_root + str_values + '/' + date + '_' + str(p_id) + '/'
    try:
        os.makedirs(ckpt_d, exist_ok=False)
    except:
        print('[WARNING] %s Already exists. Skypping run.' % ckpt_d)
        return None
            
    print('GPU:', os.environ['CUDA_VISIBLE_DEVICES'],
          '\nHyperparameter ','_'.join(grid_values.keys()), str_values,
          '\nCheckpoint directory: ', ckpt_d)

        
    
    values = {k: v for k,v in zip(grid_values.keys(), list_values)}

    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True
    tf.reset_default_graph() 
    with tf.Session(config=config) as sess:
        start = time()
        model = globals()[values['model']]
        gan = model(sess, z_dim=values['zdim'])
        
        gan.train_param.update({'g_train_steps':   values['gsteps'],
                                'dynamic_g_steps': values['gsteps']>2}) # HERE
        
        gan.train(**train_data[values['model']], # epochs=2, # HERE
                  batch_size     = values['bs'],
                  learning_rate  = values['lr'],
                  checkpoint_dir = ckpt_d) 

        print('#%i\tTraining done in %.2f.'% (p_id,time()-start))
        
        start =time()
        
        rep_out = gan.reproduce(real_images=test_images, real_labels=test_labels)
        
        if isinstance(rep_out, tuple):
            rep_images = rep_out[0]
            rep_labels = np.argmax(rep_out[1], axis=1)
        else:
            rep_images = rep_out
            rep_labels = np.zeros(len(rep_images), dtype=np.int)
            
        np.save(ckpt_d+'/test_rep_images', rep_images)
        np.save(ckpt_d+'/test_rep_labels', rep_labels)
    
        err = np.sum(np.abs(test_images - rep_images), axis=(1,2))
        eval_d = collections.OrderedDict({
                  'rel_L1': np.mean(err/np.sum(np.abs(test_images), axis=(1,2))),
                  'sum_L1':      np.sum(err),
                  'lab_acc':     np.mean(np.equal(test_labels, rep_labels))
                 })
        
        test_scores = gan.discriminator_score(test_images, labels=test_labels)
        if isinstance(test_scores, tuple):
            test_scores = test_scores[0]
            
        gen_images, gen_scores = gan.create(n_images=n_images, labels=new_labels)
        gen_scores  = gen_scores.reshape(-1,1)
        y_true  = np.vstack([np.ones_like(test_scores), np.zeros_like(gen_scores)])
        y_score = np.vstack([test_scores, gen_scores])
        TS_roc  = roc_auc_score(y_true,y_score)
        eval_d.update({'TS_roc': TS_roc})
        
        sparsity = np.mean(np.std(gen_images, axis=0))*100
        max_sum  = np.max(np.sum(gen_images, axis=(1,2)))
        eval_d.update({'sparsity': sparsity,
                       'max_sum':  max_sum})
        print('#%i\t%s\nDone in %.2f.' %
               (p_id, '\t'.join([k+': %.2f' % v  for k, v in eval_d.items()]),
                                                                   time()-start))
    return eval_d, list_values, ckpt_d

if __name__ == '__main__':
    
    NUM_GPU = len(os.environ['CUDA_VISIBLE_DEVICES'].split(','))
    print('No GPUs:', NUM_GPU)
    print('DATASET:', dataset)
    print('Checkpoint root:', ckpt_root)
    print('Repeat:', N_repeat)
    
    os.makedirs(ckpt_root, exist_ok=True)

    print('GRID SEARCH - Hyperparameters lists:')
    for k in grid_values.keys():
        print(k, grid_values[k])

    
    product = list(itertools.product(*grid_values.values())) * N_repeat
    header = True
    with multiprocessing.Pool(processes=NUM_GPU) as pool:
        with open(log_file, 'a', buffering=1) as f:
            for res in pool.imap_unordered(run_hyperparams, product, chunksize=1):
                if header:
                    eval_to_print = list(res[0].keys())
                    f.write('\t'.join(eval_to_print)+ '\t' +
                            '\t'.join([k for k in grid_values.keys()]) +
                            '\tcheckpoint_dir\n')
                    header = False
                if res:
                    f.write('\t'.join(['%.2f'%res[0][k] for k in eval_to_print]))
                    f.write('\t'+'\t'.join([str(v) for v in res[1]]))
                    f.write('\t'+res[2]+'\n')
