import logging
import sys
from GAN.model import DCGAN
from GAN.conditional.model import CDCGAN
from GAN.ACGAN.model import ACDCGAN

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)
# logging.basicConfig(level=logging.INFO)
