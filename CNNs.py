import numpy as np
# import tensorflow.compat.v1 as tf
import tensorflow as tf
from GAN.ops import *
from tensorflow.keras.layers import UpSampling2D
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class BaseCNN():
    def __init__(self, n_channels, kernel_sizes, bn_layers, name, image_size,
                 **kwargs):
        '''
        Initialize BaseCNN. This implementation requires image_size to be a
        multiple of 2^(N conv layers).
        Arguments: n_channels, kernel_sizes, bn_layers, name, image_size
        '''
        super().__init__(**kwargs)
        self.n_channels   = n_channels
        self.kernel_sizes = kernel_sizes
        self.name         = name
        
        self.image_size   = image_size
        self.small_size   = image_size//2**(len(n_channels)-1)
        
        assert image_size % 2**(len(n_channels)-1) == 0
        
        with tf.variable_scope(self.name) as scope:
            self.scope_name = scope.original_name_scope
            #Initialize all weights and bias
            logger.info("Initializing %s weights..."% self.name)
            self.W = []
            self.b = []
            for i in range(len(self.n_channels)-1):
                W_shape = [self.kernel_sizes[i], self.kernel_sizes[i],
                           self.n_channels[i], self.n_channels[i+1]]
                b_shape = [self.n_channels[i+1]]
                self.W.append(init_weights(W_shape))
                self.b.append(init_bias(b_shape))
                
        #Create Batch Normalisation Object for each layer in bn_layers
        self.bns = {i: batch_norm(name='%sbn%d'%(self.name, i)) for i in bn_layers}

    
    def conv_layer(self, i, _input, stride=2, activation=tf.nn.leaky_relu):
        with tf.variable_scope('conv_layer'):
            s_shape = [1, stride, stride, 1]
            _conv   = conv2d(_input, self.W[i], s_shape, padding="SAME") 
            _out    = tf.nn.bias_add(_conv, self.b[i])

            if i in self.bns.keys():
                _out = self.bns[i](_out, training=self.is_training)

            if activation:
                _out = activation(_out)

            logger.info('%s%d %s'% (self.name, i, str(np.shape(_out))))
            return _out
            
    def dense(self, _input, W, b, size):
        with tf.variable_scope('dense'):
            _flat    = tf.matmul(_input, W)   
            _reshape = tf.reshape(_flat, [-1, size, size, b.shape[0]])
            _bias    = tf.nn.bias_add(_reshape, b)
            _lrelu   = tf.nn.leaky_relu(_bias)
            logger.info('%sdense %s'% (self.name, str(np.shape(_lrelu))))
        return _lrelu
    
    def __call__(self, _tensor, training, reuse=False): 
        self.is_training = training
        
        with tf.variable_scope(self.scope_name) as scope:     
            if reuse:
                scope.reuse_variables()
            
            for i in range(len(self.n_channels)-1):
                _tensor = self.conv_layer(i, _tensor)
                
            return _tensor
    
class Discriminator(BaseCNN):
    def __init__(self, **kwargs):
        super().__init__(n_channels   = [1, 16, 32, 64, 128],
                         kernel_sizes = [3, 3, 3, 3],
                         bn_layers    = [1, 2, 3],
                         name         = 'disc_',
                         **kwargs)
        self.len_linear = self.small_size**2*self.n_channels[-1]
        with tf.variable_scope(self.scope_name):
            self.W.append(init_weights([self.len_linear, 1]))
            self.b.append(init_bias([1]))
        
        
    def __call__(self, X, training, reuse=False, **kwargs):
        logger.info('%sinput %s'% (self.name, str(np.shape(X))))
        
        #4 subsequent convolutional layers
        conv3 = super().__call__(X, training, reuse)
            
        with tf.variable_scope(self.scope_name) as scope:
            if reuse:
                scope.reuse_variables()
                
            #Fully Connected output layer
            self.flat   = tf.reshape(conv3, [-1, self.len_linear])
            logits      = tf.matmul(self.flat, self.W[4])
            logits_bias = tf.nn.bias_add(logits, self.b[4])
            logger.info('%slogits %s'% (self.name, str(np.shape(logits_bias))))

        return tf.math.sigmoid(logits_bias), logits_bias
    
    def compute_size(self, i_layer):
        return self.image_size//2**i_layer

class Generator(BaseCNN):
    def __init__(self, z_dim, **kwargs):
        '''
        Initialize Generator. 
        Arguments: z_dim
        '''
        super(Generator, self).__init__(n_channels   = [128, 64, 32, 16, 1],
                                        kernel_sizes = [5, 5, 5, 5],
                                        bn_layers    = [0, 1, 2],
                                        name         = 'gen_',
                                        **kwargs)
                                
        self.len_linear = self.small_size**2*self.n_channels[0]
                                
        with tf.variable_scope(self.scope_name):
            self.W_lin = init_weights([z_dim, self.len_linear])
            self.b_lin = init_bias([self.n_channels[0]])


    def conv_layer(self, i, _input):
        ups_input = UpSampling2D()(_input)
        
        if i != 3:
            _out = super().conv_layer(i, ups_input, stride=1)
        else:
            _out = super().conv_layer(i, ups_input, stride=1,
                                      activation=tf.nn.sigmoid)
        return _out
        
    def __call__(self, z, training, reuse=False, **kwargs):
        logger.info('%sinput %s'% (self.name, str(np.shape(z))))
            
        with tf.variable_scope(self.scope_name) as scope:
            if reuse:
                scope.reuse_variables()
                
            #Fully Connected input layer
            z0 = self.dense(z, self.W_lin, self.b_lin, self.small_size)
            
        #4 subsequent convolutional layers
        z4 = super().__call__(z0, training, reuse)
        return z4
    
    def compute_size(self, i_layer):
        return self.small_size*2**i_layer