import numpy as np
import tensorflow as tf
from tensorflow.python.framework import ops
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def loc_norm(x):
    '''
    Perform local normalization of a dataset.
    '''
    x_min = np.amin(x, axis=(1,2), keepdims=True)
    x_dif = np.ptp(x, axis=(1,2), keepdims=True)
    return (x - x_min) / x_dif

def load_data(data, preproc=None):
    '''
    Load and preprocess data.
    data: array or path where to load data.
    preproc: optional fuction to preprocess data (e.g. preproc=loc_norm)
    '''
    if isinstance(data, str):
        logger.info('Loading data from:\n%s' % data)
        data = np.load(data)
    if preproc:
        data = preproc(data)
    return data
    
class Labelled_Minibatch_Iterator:
    """
    Class defining a minibatch iterator of labelled images. At each initialisation (iter)
    the data are shuffled (per label). The next method provide a balanced batch containg
    batch_size images per label. Thus:
    
            real_batch_size = batch_size * N_labels
    
    The output has always the same labels encoded as one_hot vectors in the labels array
    of shape (batch_size, n_labels). The number of mini batches created depends on the 
    number of images of the less represented label.
    
    preproc: optional fuction to preprocess data (e.g. preproc=loc_norm)
    """
    def __init__(self, data, labels, batch_size, shuffle=True, preproc=None, **kwargs):
        data   = load_data(data, preproc=preproc)
        data   = np.expand_dims(data, -1)
        labels = load_data(labels)
        assert data.shape[0] == labels.shape[0]
        
        self.n_labels  = len(np.unique(labels))
        assert batch_size%self.n_labels == 0
        self.data_dict = {}
        for l in range(self.n_labels):
            self.data_dict[l] = data[labels==l]
        
        self.min_l     = min([len(d) for d in self.data_dict.values()])
        self.label_bs  = batch_size//self.n_labels
        self.n_batches = self.min_l//self.label_bs
        self.shuffle   = shuffle
        labels         = np.repeat(np.arange(self.n_labels), self.label_bs)
        self.one_hot   = np.eye(self.n_labels)[labels]
        
        logger.info('Data shape %s'%str(data.shape))
        logger.info('Labels found: %d' % self.n_labels)
        for l in range(self.n_labels):
            logger.info('Label %d: %d' %(l, len(self.data_dict[l])))
        logger.info('Labels shape %s'%str(labels.shape))
        logger.info('N batches: %d' % self.n_batches)

            
    
    def __iter__(self):
        if self.shuffle:
            for l in range(self.n_labels):
                np.random.shuffle(self.data_dict[l])
        self.gen = iter(range(0, self.min_l-self.label_bs+1, self.label_bs))
        return self

    def __next__(self):
        i     = next(self.gen)
        batch = np.vstack([d[i:i+self.label_bs] for d in self.data_dict.values()])
        return batch, self.one_hot
    
class Minibatch_Iterator:
    """
    Class defining a minibatch iterator. At each initialisation (iter) the data
    are shuffled. The next method provide the next batch.
    
    data: array or path where to load it.
    preproc: optional fuction to preprocess data (e.g. preproc=loc_norm)
    """
    def __init__(self, data, batch_size, shuffle=True, preproc=None, **kwargs):
        data            = load_data(data, preproc=preproc)
        self.data       = np.expand_dims(data, -1)
        self.batch_size = batch_size
        self.n_batches  = len(self.data)//self.batch_size
        self.shuffle    = shuffle
        
        logger.info('Data shape %s'%str(self.data.shape))
        logger.info('N batches: %d' % self.n_batches)

    def __iter__(self):
        if self.shuffle:
            np.random.shuffle(self.data)
        self.gen = iter(range(0, len(self.data)-self.batch_size+1, self.batch_size))
        return self

    def __next__(self):
        i     = next(self.gen)
        batch = self.data[i:i+self.batch_size]
        return batch
    

class batch_norm(object):
    """
    Class to perform Batch Normalisation. DEPRECATED!
    Code modification of http://stackoverflow.com/a/33950177
    """
    def __init__(self, epsilon=1e-5, momentum = 0.9, name="batch_norm"):
        with tf.variable_scope(name):
            self.epsilon = epsilon
            self.momentum = momentum

            self.name = name

    def __call__(self, x, training):
        return tf.contrib.layers.batch_norm(x, decay=self.momentum, updates_collections=None,
                                            epsilon=self.epsilon, center=True, scale=True,
                                            is_training=training, scope=self.name)


def init_weights(shape):
    '''Weights initialisation'''
    return tf.Variable(tf.random.normal(shape=shape, stddev=0.02))


def init_bias(shape):
    '''Bias initialisation'''
    return tf.Variable(tf.zeros(shape))


def conv2d(x, filter, strides, padding):
    '''Convolutional layer creation'''
    return tf.nn.conv2d(x, filter, strides=strides, padding=padding)


def cost(labels, logits):
    '''Cost function'''
    return tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=labels, logits=logits))
