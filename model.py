import os
import time
import tensorflow as tf
from GAN.ops import *
from GAN.CNNs import *
from tensorflow.nn import sigmoid_cross_entropy_with_logits as sig_Xentropy

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class DCGAN(object):
    def __init__(self, sess, z_dim=100, flip_prob=0.1, soft_err=0.05, image_size=64):
        """
        Args:
            sess: TensorFlow session
            data_path: Path where to load training data (file or folder).
            checkpoint_dir: Directory where to store the checkpoints.
            z_dim: (optional) Dimension of dim for Z. [100]
            image_size: For the current implementation only multiple of 32 are allowed
        """
        self.sess        = sess

        # Network parameters
        self.z_dim       = z_dim
        self.z_stddev    = 0.3

        # Soft&flip scores
        self.flip_prob   = flip_prob
        self.soft_err    = soft_err
        
        # Image size (only multiple of 32 are allowed)
        self.image_size  = image_size
        self.image_shape = [self.image_size, self.image_size, 1]
        
        # Other params' dictionaries
        self.sum_param   = {'summary_steps': 500,
                            'max_gen_imgs': 10,
                            'max_val_imgs': 20,
                            'epochs_save': 10}
        
        self.train_param = {'decay_steps': 1000,
                            'decay_rate': 0.99,
                            'beta1': 0.5,
                            'g_train_steps': 1,
                            'dynamic_g_steps': False}
                            
        self.gen_param   = {'n_replicas': 16,
                            'lam': 0.1,
                            'n_iter': 0,
                            'learning_rate': 0.001,
                            'decay_steps': 100,
                            'decay_rate': 0.98,
                            'rep_batch': 10,
                            'initial_value': None}
        
        self.model_name  = "DCGAN"
        
        self.generator      = self.build_generator()
        self.discriminator  = self.build_discriminator()
        self.minibatch_iter = Minibatch_Iterator 
        self.build_model()
        
    def z_init(self, batch_size):
        shape = [batch_size, self.z_dim]
        return np.random.normal(0, self.z_stddev, shape).astype(np.float32)

    def build_generator(self):
        return Generator(self.z_dim, image_size=self.image_size)
    
    def build_discriminator(self):
        return Discriminator(image_size=self.image_size)
        
    def build_model(self, **kwargs):
        self.is_training = tf.placeholder(tf.bool, name='is_training')
        
        self.z      = tf.placeholder(tf.float32, [None, self.z_dim], name='z')
        self.images = tf.placeholder(tf.float32, [None]+self.image_shape, name='real_images')
        self.G      = self.generator(self.z, training=self.is_training, **kwargs)
        
        # Real and Synthetic_ images score
        self.D_out  = self.discriminator(self.images, training=self.is_training,
                                                   **kwargs)
        self.D__out = self.discriminator(self.G, training=self.is_training,
                                                   reuse=True, **kwargs)
        self.D      = self.D_out[0]
        self.D_log  = self.D_out[1]
        self.D_     = self.D__out[0]  
        self.D_log_ = self.D__out[1]

        self.define_loss()
        
    def define_loss(self):
        z_sum  = tf.summary.histogram("z", self.z)
        d_sum  = tf.summary.histogram("d", self.D)
        d__sum = tf.summary.histogram("d_", self.D_)
        G_sum  = tf.summary.image("G", self.G, max_outputs=self.sum_param['max_gen_imgs'])
        
        # Define real and syntetic scores for the Discriminator
        with tf.variable_scope('D_scores'):
            self.real_s = tf.ones_like(self.D) 
            self.synt_s = tf.zeros_like(self.D_)

            # Soft scores (by applying gaussian noise to 1s and 0s)
            self.real_s -= tf.abs(tf.truncated_normal(tf.shape(self.D), stddev=self.soft_err))
            self.synt_s += tf.abs(tf.truncated_normal(tf.shape(self.D_), stddev=self.soft_err))

            # Flip scores 
            flip_mask   = tf.greater(tf.random.uniform(tf.shape(self.real_s)), self.flip_prob)
            self.real_s = tf.where(flip_mask, self.real_s, tf.ones_like(self.real_s)-self.real_s)
            flip_mask   = tf.greater(tf.random.uniform(tf.shape(self.synt_s)), self.flip_prob)
            self.synt_s = tf.where(flip_mask, self.synt_s, tf.ones_like(self.synt_s)-self.synt_s)

        # Loss functions
        self.d_loss_real = tf.reduce_mean(sig_Xentropy(logits=self.D_log,
                                                       labels=self.real_s))
        self.d_loss_fake = tf.reduce_mean(sig_Xentropy(logits=self.D_log_,
                                                       labels=self.synt_s))
        self.d_loss      = self.d_loss_real + self.d_loss_fake
        self.g_loss      = tf.reduce_mean(sig_Xentropy(logits=self.D_log_,
                                                       labels=tf.ones_like(self.D_)))

        d_loss_real_sum = tf.summary.scalar("d_loss_real", self.d_loss_real)
        d_loss_fake_sum = tf.summary.scalar("d_loss_fake", self.d_loss_fake)
        d_loss_sum      = tf.summary.scalar("d_loss", self.d_loss)
        g_loss_sum      = tf.summary.scalar("g_loss", self.g_loss)
                
        self.g_sum = tf.summary.merge([z_sum, d__sum, G_sum, d_loss_fake_sum, g_loss_sum])
        self.d_sum = tf.summary.merge([z_sum, d_sum, d_loss_real_sum, d_loss_sum])

        t_vars      = tf.trainable_variables()

        self.d_vars = [var for var in t_vars if 'disc_' in var.name]
        self.g_vars = [var for var in t_vars if 'gen_' in var.name]

        self.saver = tf.train.Saver(max_to_keep=1)
        
    
    
    def build_generation_model(self, n_images, **kwargs):
        '''
        The generation model is different from the one for training: z is now a variable.
        This is used later for reproduction and creation of new images. 
        '''
        with tf.name_scope("generate") as scope:
            self.z_var     = tf.Variable(self.z_init(n_images), name='z_var')          
            self.G_gen     = self.generator(self.z_var, training=self.is_training,
                                            reuse=True, **kwargs)
            self.D_gen_out = self.discriminator(self.G_gen, training=self.is_training,
                                                reuse=True, **kwargs)
            self.D__gen    = self.D_gen_out[0]
            self.D_log_gen = self.D_gen_out[1]

            return scope
            
    def build_creation_model(self, n_images, **kwargs):
        '''
        Defines the loss function and the training op to maximise the D score of
        the generated images.
        '''
            
        with tf.name_scope("create") as scope:
            self.build_generation_model(n_images, **kwargs)
            cre_step  = tf.Variable(0)
            creation_loss = tf.reduce_mean(sig_Xentropy(logits=self.D_log_gen,
                                                        labels=tf.ones_like(self.D_log_gen)))

            #calculate the decaying learning rate over time
            cre_lr = tf.train.exponential_decay(self.gen_param['learning_rate'],
                                                cre_step,
                                                self.gen_param['decay_steps'],
                                                self.gen_param['decay_rate'],
                                                name="learning_rate", staircase=True)

            cre_optim = tf.train.AdamOptimizer(cre_lr)
            self.train_cre = cre_optim.minimize(creation_loss, var_list=self.z_var)
        return scope
            
    
    def build_reproduction_model(self, rep_batch=10, **kwargs):
        
        # validataion data and parameters
        self.rep_batch = rep_batch
        
        with tf.name_scope("reproduce") as scope:
            self.build_generation_model(self.gen_param['n_replicas']*rep_batch, **kwargs)
            # Reproduction model
            rep_step = tf.Variable(0)
 
            G_rep     = self.G_gen
            D_log_rep = self.D_log_gen
        
            
            # Loss functions:
            # per replica = [relative] L1 error + lambda * perceptual Loss
            rep_diff   = tf.reduce_sum(tf.abs(G_rep - self.images), axis=(1,2,3))
            rep_r_diff = rep_diff / tf.reduce_sum(tf.abs(self.images), axis=(1,2,3))
            rep_D_loss = sig_Xentropy(logits=D_log_rep, labels=tf.ones_like(D_log_rep))
            rep_complete_loss = rep_r_diff + self.gen_param['lam'] * tf.reshape(rep_D_loss, [-1])
            # per batch
            rep_loss   = tf.reduce_sum(rep_complete_loss)
            
            # Resahpe losses and replicas (array -> grid)
            grid_shape = [rep_batch, self.gen_param['n_replicas']]
            rep_diff   = tf.reshape(rep_diff, grid_shape)
            rep_r_diff = tf.reshape(rep_r_diff, grid_shape)
            G_rep      = tf.reshape(G_rep, grid_shape + self.image_shape)
            
            # Sparsity of reproduction
            rep_spars  = tf.reduce_mean(tf.math.reduce_std(G_rep, axis=0))
            
            rep_complete_loss = tf.reshape(rep_complete_loss, grid_shape)
            
            # Get best replica for each real image (minimum loss along grid rows)
            col_argmin    = tf.math.argmin(rep_complete_loss, axis=1)
            self.list_ind = tf.stack([tf.range(rep_batch, dtype=tf.int64), col_argmin], axis=1)
            
            G_best_rep      = tf.gather_nd(G_rep, self.list_ind)
            rep_diff_best   = tf.gather_nd(rep_diff, self.list_ind)
            rep_diff_best   = tf.reduce_mean(rep_diff_best)
            rep_r_diff_best = tf.gather_nd(rep_r_diff, self.list_ind)
            rep_r_diff_best = tf.reduce_mean(rep_r_diff_best)
            
            self.G_rep_out  = [G_best_rep]
            
            # Summaries 
            # rep_diff: mean L1 error (real - best replica) over real images
            # rep_loss: sum L1 error (real - replica) over ALL replicas
            # rep: Best replicas
            rep_diff_sum   = tf.summary.scalar("rep_diff", rep_diff_best)
            rep_r_diff_sum = tf.summary.scalar("rep_relative_diff", rep_r_diff_best)
            rep_loss_sum   = tf.summary.scalar("rep_loss", rep_loss)
            rep_spars_sum  = tf.summary.scalar("rep_sparsity", rep_spars)
            rep_img_sum    = tf.summary.image("rep", G_best_rep, max_outputs=rep_batch)
            self.rep_sum   = tf.summary.merge([rep_diff_sum, rep_r_diff_sum,
                                               rep_loss_sum, rep_img_sum, rep_spars_sum])

            # Define optimizers and train op
            with tf.name_scope("train"):
                rep_learning_rate = tf.train.exponential_decay(self.gen_param['learning_rate'],
                                                rep_step,
                                                self.gen_param['decay_steps'],
                                                self.gen_param['decay_rate'],
                                                name="learning_rate", staircase=True)

                rep_opt           = tf.train.AdamOptimizer(rep_learning_rate)
                self.train_rep    = rep_opt.minimize(rep_loss, var_list=self.z_var,
                                                     global_step = rep_step)
                
        return scope
    
    def load_validation_data(self, val_data, **kwargs):
        val_batch_iter = self.minibatch_iter(val_data, self.sum_param['max_val_imgs'],
                                             shuffle=False, **kwargs)
        val_batch_data = next(iter(val_batch_iter))
        return val_batch_data
    
    def validation_args(self, val_batch_data):
        return {self.images: val_batch_data}
        
    def train_feed_dict(self, batch_data):
        batch_size = batch_data.shape[0]
        batch_z    = self.z_init(batch_size)
        feed_dict  = {self.images: batch_data, self.z: batch_z,
                      self.is_training: True}
        return feed_dict

    def train(self, data, epochs=50, batch_size=16, learning_rate=0.0005,
              checkpoint_dir='', init=True, val_data=None, **kwargs):

        # get number of batches
        batch_iter = self.minibatch_iter(data=data, batch_size=batch_size,
                                         **kwargs)
        n_batches  = batch_iter.n_batches
        
        #calculate the decaying learning rate over time
        with tf.name_scope("learning_rate"):
            batch_id    = tf.Variable(0)
            global_step = batch_size * batch_id
            learn_rate  = tf.train.exponential_decay(learning_rate, global_step,
                                                     self.train_param['decay_steps'],
                                                     self.train_param['decay_rate'],
                                                     name="learning_rate",
                                                     staircase=True)

        lr_log = tf.summary.scalar("learning_rate", learn_rate)
        writer = tf.summary.FileWriter(os.path.join(checkpoint_dir, "logs"),
                                       self.sess.graph)
        
        with tf.name_scope("optim"):
            d_optim = tf.train.AdamOptimizer(learn_rate, self.train_param['beta1'])
            g_optim = tf.train.AdamOptimizer(learn_rate, self.train_param['beta1'])
            
            d_train = d_optim.minimize(self.d_loss, var_list=self.d_vars, 
                                            global_step=batch_id)
            g_train = g_optim.minimize(self.g_loss, var_list=self.g_vars)
        
        if val_data is not None:
            # build the reproduction model and create a validation set minibatch iterator
            self.build_reproduction_model(rep_batch=self.sum_param['max_val_imgs'],
                                          **kwargs)
            val_batch_data = self.load_validation_data(val_data, **kwargs)
            val_args = self.validation_args(val_batch_data)

        if init:
            tf.global_variables_initializer().run()
        else:
            tf.initializers.variables(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES,
                                                        scope='learning_rate')).run()
            tf.initializers.variables(d_optim.variables()).run()
            tf.initializers.variables(g_optim.variables()).run()

        counter           = 1
        processed_images  = 0
        summary_update    = self.sum_param['summary_steps']
        g_train_steps = self.train_param['g_train_steps']
        
        logger.info('g_train_steps: %d '% g_train_steps)
        start_time = time.time()

        for epoch in range(epochs):
            logger.info('EPOCH: %d'%epoch)
            
            # g_steps summary
            g_steps_sum = tf.Summary()
            g_steps_sum.value.add(tag='g_train_steps', simple_value=g_train_steps)
            writer.add_summary(g_steps_sum, processed_images)

            # Accumulators for g_train_steps statistics
            sum_D = 0
            sum_D_ = 0
            
            # Data are automatically shuffled at every initialisation of this loop
            for idx, batch_data in enumerate(batch_iter):

                feed_dict = self.train_feed_dict(batch_data)

                # Update D network
                _, D, d_summary_str = self.sess.run([d_train, self.D, self.d_sum],
                                                       feed_dict=feed_dict)
                sum_D += np.mean(D)
                
                # Update G network by training it g_train_steps times
                for _ in range(g_train_steps):
                    batch_z = self.z_init(batch_size)
                    feed_dict.update({self.z: batch_z})
                    _, D_, g_summary_str = self.sess.run([g_train, self.D_, self.g_sum],
                                                            feed_dict=feed_dict)
                    sum_D_ += np.mean(D_) / g_train_steps
                
                # Update summaries and validate after processing summary_steps images
                processed_images += batch_size
                if processed_images >= summary_update:
                    lr_summary_str = lr_log.eval()
                    writer.add_summary(lr_summary_str, processed_images)
                    writer.add_summary(g_summary_str, processed_images)
                    writer.add_summary(d_summary_str, processed_images)
                    summary_update += self.sum_param['summary_steps']
                    
                    # Validation by reproduction
                    if val_data is not None:
                        *_, rep_sum_str = self.reproduce_batch(batch_args=val_args)
                        writer.add_summary(rep_sum_str, processed_images)
                    
                    
                feed_dict.update({self.is_training: False})
                errD = self.d_loss.eval(feed_dict)
                errG = self.g_loss.eval(feed_dict)

                logger.debug("Epoch: [%d] [%d/%d] time: %.3f, d_loss: %.3f, g_loss: %.3f"%
                             (epoch, idx, n_batches, time.time() - start_time, errD, errG))
                counter += 1
                
            if self.train_param['dynamic_g_steps']:
                g_train_steps = self.train_param['g_train_steps']*(sum_D-sum_D_)/n_batches
                g_train_steps = int(max(1, np.ceil(g_train_steps)))
                logger.info('g_train_steps: %d '% g_train_steps)
            
    
            if epoch % self.sum_param['epochs_save'] == 0:
                self.save(checkpoint_dir, counter)
        self.save(checkpoint_dir, counter)
                
    def reproduce_batch(self, batch_args, **kwargs):
        '''
        Produces a replica for each image in batch_images using the pre-built model
        Arguments:
        - batch_images: array of shape (self.rep_batch, self.image_size, self.image_size, 1);
        Returns:
        - G_imgs: array with the same shape of batch_images containing the best replicas;
        - G_lab: 2D one-hot vector encoding the labels of the best replicas;
        - rep_sum_str: summary.
        '''

        # Initialize variables (this avoid pblms with AdamOpt)
        self.sess.run(
            tf.initializers.variables(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES,
                                                                  scope='reproduce')))
        # If provided, fix the initial value 
        if self.gen_param['initial_value'] is not None:
            assign_op = self.z_var.assign(self.gen_param['initial_value'])
            self.sess.run(assign_op)

        feed_dict = {k: np.repeat(batch_args[k], [self.gen_param['n_replicas']], axis=0)
                         for k in batch_args.keys()}
        feed_dict.update({self.is_training: False})
        rep_out, rep_sum_str = self.sess.run([self.G_rep_out, self.rep_sum],
                                                   feed_dict=feed_dict)
        # Train z
        for i in range(self.gen_param['n_iter']):
            rep_out, rep_sum_str, _ = self.sess.run([self.G_rep_out, self.rep_sum,
                                                     self.train_rep],
                                                    feed_dict=fd)
        # logger report
        self.batch_reproduction_log(batch_args, rep_out)
        return rep_out, rep_sum_str
    
    def batch_reproduction_log(self, batch_args, G_out):
        diff  = np.sum(np.abs(batch_args[self.images] - G_out[0]), axis=(1,2,3))
        logger.info("L1-error mean (min / max): %.2f (%.2f / %.2f)"
                                          % (np.mean(diff), np.min(diff), np.max(diff)))
        
    def batch_iterator(self, real_images, rep_batch, **kwargs):
        missing_data = rep_batch - 1 - (real_images.shape[0] - 1) % rep_batch
        data         = real_images.reshape([-1] + self.image_shape)
        data         = np.vstack([data, np.zeros([missing_data] + self.image_shape)])
        return iter(data[i: i + rep_batch] for i in range(0, data.shape[0], rep_batch))
    
    def collect_data(self, list_out, shape):
        best_replicas = np.vstack([batch_out[0] for batch_out in list_out])
        return best_replicas[:shape[0]].reshape(shape)
        
        
    def reproduce(self, real_images, **kwargs):
        '''
        Builds a new reproduction model and produce a replica for each image in real_images.
        If n_iter = 0, there is no training on z.
        Arguments:
        - real_images: array of shape(-1, self.image_size, self.image_size, 1);
        - n_replicas: Number of replicas per image [500];
        - n_iter: number of training iterations [0];
        - rep_batch: batch size to consider during reproduction [10];
        - learning_rate: learning rate to use during reproduction [0.001];
        - decay_steps: check tf.train.exponential_decay [100];
        - decay_rate: check tf.train.exponential_decay [0.98];
        - initial_value: specifies the initial value of the latent variable z [None].
        Returns:
        - best_replicas: array with the same shape of real_images containing the best 
                         replica for each image;
        - best_labels: 2D one-hot vector encoding the labels of the best replicas.
        '''
        self.build_reproduction_model(rep_batch=self.gen_param['rep_batch'], **kwargs)
        batch_iter = self.batch_iterator(real_images, self.gen_param['rep_batch'], **kwargs)
        
        list_out = []

        for i, batch_data in enumerate(batch_iter):
            batch_args = self.validation_args(batch_data)
            logger.info('reproducing batch: %d' % i)
            batch_rep_out, *_ = self.reproduce_batch(batch_args=batch_args)
            list_out.append(batch_rep_out)

        best_replicas = self.collect_data(list_out, real_images.shape)
        return best_replicas
    

    def create(self, n_images, **kwargs):
        '''
        Produces synthetic images. If n_iter is a positive integer the latent variable z
        is trained to maximise the discriminator score of the images generated.
        Arguments:
        - n_images: int, number of images to create;
        - n_iter: number of training iterations [100];
        - learning_rate: learning rate to use [0.001];
        - decay_steps: check tf.train.exponential_decay [100];
        - decay_rate: check tf.train.exponential_decay [0.98];
        - initial_value: specifies the initial value of the latent variable z [None].
        Returns:
        - fake_images: array with shape (n_images, image_size, image_size) containing fake images.
        '''
        self.build_creation_model(n_images, **kwargs)   
        # Initialize variables (this avoid pblms with AdamOpt)
        self.sess.run(tf.initializers.variables(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES,
                                                                  scope='create')))
        # If provided, fix the initial value 
        if self.gen_param['initial_value'] is not None:
            assign_op = z_var.assign(self.gen_param['initial_value'])
            self.sess.run(assign_op)
        
        fd = {self.is_training: False}

        G_imgs, D_ = self.sess.run([self.G_gen, self.D__gen], feed_dict=fd)
        
        # Train z
        for i in range(self.gen_param['n_iter']):
            G_imgs, D_, _ = self.sess.run([self.G_gen, self.D__gen, self.train_cre],
                                          feed_dict=fd)
            if i % 100 == 0:
                logger.debug('iteration %d: D_ = %.4f'%(i, D_[0][0]))
        
        return G_imgs.reshape((-1,self.image_size, self.image_size)), D_.reshape((-1,))
    
    
    def discriminator_score(self, images, **kwargs):
        '''
        Return the discriminator score for the input images
        Arguments:
        - images: array of images;
        Returns:
        - scores: array of lenght len(images) containing the score of each image.
        '''
        images = images.reshape([-1] + self.image_shape)
        scores = self.D.eval(feed_dict={self.images: images,
                                        self.is_training: False})
        return scores

    def save(self, checkpoint_dir, step):
        if not os.path.exists(checkpoint_dir):
            os.makedirs(checkpoint_dir)

        self.saver.save(self.sess,
                        os.path.join(checkpoint_dir, self.model_name),
                        global_step=step)

    def load(self, checkpoint_dir):
        logger.info(" [*] Reading checkpoints...")
        
        ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
        if ckpt and ckpt.model_checkpoint_path:
            self.saver.restore(self.sess, ckpt.model_checkpoint_path)
            return True
        else:
            return False
    
    def __str__(self):
        return self.model_name