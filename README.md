## Python Package implementing various GAN models

#### DCGAN
This is the base class, all other classes inherit from it.

#### CDCGAN
Conditional version of a DCGAN: both Generator and Discriminators require labels as input.

#### ACDCGAN
Auxiliary Classifier DCGAN: the Generator produce images given an input label while the Discriminator learn to classify the labels of the input images.

#### Grid_search
Leveraging the common APIs this script performs a hyperparameter grid search using different models. Run on multiple GPUs when available. 

## APIs
All APIs can be used in the same way for the 3 models. The only differences are in the number of values returned by the `reproduce` and `discriminator_score`. When labelled data are passed to the methods of the `DCGAN` the labels are just ignored and no error will appear.

#### Initialise
To initialise a model first start a tensorflow session and pass it to the model constructor.
```
from GAN import DCGAN, CDCGAN, ACDCGAN
import tensorflow as tf

model = ACDCGAN # Chose which model to use (DCGAN, CDCGAN, or ACDCGAN)
with tf.Session() as sess:
    # Initialize the model
    gan = model(sess)
```

#### Train or load
Once the gan is initilised it can be trained,
```
    gan.train(data='path/to/train_images',
              labels='path/to/train_labels',
              checkpoint_dir='path/where/to/store/model')
```
or we can load its weight from the checkpoint of a similar model (with the same structure!).
```
    gan.load('path/where/to/load/model')
```

#### Synthetic Images
To create replicas of real images use the `reproduce` method.
```
    rep_out = gan.reproduce(real_images=test_data, real_labels=test_labels)
```
Note that:
* For `CDCGAN` and `ACDCGAN` `rep_out` is a tuple contaning two numpy arrays (the synthetic images and their labels), while for `CDCGAN` it is an array of images.
* The `real_labels` argument is used to display some statistics and does not specify the labels of the synthetic images.

To create new images use the `create` method.
```
    new_images, new_scores = gan.create(n_images=n_images, labels=new_labels)
```
In this case `labels` specifies the label of the synthetic images. The return values are the new images and their Discriminator score.

#### Discriminator Score
To get the _'reality'_ score of an image use the `discriminator_score` method.
```
    D_out = gan.discriminator_score(images, labels=labels)
```
Note that:
* The `labels` argument is considered only in the case of a `CDCGAN` model.
* For `DCGAN` and `CDCGAN`, `D_out` is a numpy array containg the score of each image in `images`, while for `ACDCGAN` it is a tuple containg as second element the probabilities to belong to each class.

## Examples
Please refer to the `Train_Load_Reproduce` notebook ([here](../notebooks/Train_Load_Reproduce.ipynb)).
