import os
import time
import tensorflow as tf
from GAN.ops import *
from GAN.model import DCGAN
from GAN.ACGAN.CNNs import *
from tensorflow.nn import softmax_cross_entropy_with_logits as smax_Xentropy

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class ACDCGAN(DCGAN):
    def __init__(self, sess, z_dim=100, n_labels=2, flip_prob=0.1, soft_err=0.05,
                 image_size=64):        
        # Labels
        self.n_labels       = n_labels
        
        # Initialize model
        super().__init__(sess, z_dim, flip_prob, soft_err, image_size)
        
        # Labelled Iterator
        self.minibatch_iter = Labelled_Minibatch_Iterator 
        self.model_name     = "ACDCGAN"
        
    def build_generator(self):
        return AC_Generator(self.z_dim, self.n_labels, image_size=self.image_size)
    
    def build_discriminator(self):
        return AC_Discriminator(self.n_labels, image_size=self.image_size)
        
    def build_model(self):
        self.labels = tf.placeholder(tf.float32, [None, self.n_labels],
                                     name='label')
        
        super().build_model(label=self.labels)
        self.D_aux = self.D_out[-2]

    def define_loss(self):
        super().define_loss()
        self.aux_loss_real = tf.reduce_mean(smax_Xentropy(logits=self.D_out[-1],
                                                          labels=self.labels))
        self.aux_loss_fake = tf.reduce_mean(smax_Xentropy(logits=self.D__out[-1],
                                                          labels=self.labels))
        self.g_loss += self.aux_loss_fake
        self.d_loss += 2*self.aux_loss_real

        self.aux_loss_real_sum = tf.summary.scalar("aux_loss_real", self.aux_loss_real)
        self.aux_loss_fake_sum = tf.summary.scalar("aux_loss_fake", self.aux_loss_fake)
        
        self.g_sum = tf.summary.merge([self.g_sum, self.aux_loss_fake_sum])
        self.d_sum = tf.summary.merge([self.d_sum, self.aux_loss_real_sum])


    def build_generation_model(self, n_images, labels, **kwargs):
        '''
        The generation model is different from the one for training: z is now a variable.
        This is used later for reproduction and creation of new images. 
        '''
        with tf.name_scope("labels_to_generate"):
            self.l_gen = tf.constant(labels, dtype=tf.float32, name='l_gen')
        scope = super().build_generation_model(n_images, label=self.l_gen)
        return scope
   
    def build_reproduction_model(self, rep_batch=10, **kwargs):
        labels    = np.arange(self.gen_param['n_replicas'] * rep_batch)% self.n_labels
        labels_oh = np.eye(self.n_labels)[labels]
        scope = super().build_reproduction_model(rep_batch, labels=labels_oh)
        
        with tf.name_scope(scope):
            # Grid of one hot vectors, used to compute label accuracy.
            new_shape      = [rep_batch, self.gen_param['n_replicas'], self.n_labels]
            lab_best_rep   = tf.gather_nd(tf.reshape(self.l_gen, new_shape), self.list_ind)
            true_labels    = tf.gather_nd(tf.reshape(self.labels, new_shape), self.list_ind)
            correct_labels = tf.reduce_all(tf.equal(true_labels, lab_best_rep), axis=1) 
            l_acc_rep      = tf.reduce_mean(tf.cast(correct_labels, tf.float32))
            
            self.G_rep_out += [lab_best_rep]
            
            # Summaries 
            l_acc_rep_sum  = tf.summary.scalar("rep_Label_acc", l_acc_rep)
            self.rep_sum   = tf.summary.merge([self.rep_sum, l_acc_rep_sum])
        return scope
        
    def load_validation_data(self, val_data, val_labels, **kwargs):
        val_batch_iter = self.minibatch_iter(data=val_data,
                                             labels=val_labels,
                                             batch_size=self.sum_param['max_val_imgs'],
                                             shuffle=False)
        val_batch_data = next(iter(val_batch_iter))
        return val_batch_data
        
    def validation_args(self, val_batch_data):
        images = val_batch_data[0]
        labels = val_batch_data[1]
        return {self.images: images,
                self.labels: labels}        

    def train_feed_dict(self, batch_data):
        batch_size = batch_data[0].shape[0]
        batch_z    = self.z_init(batch_size)
        feed_dict  = {self.images: batch_data[0], self.labels: batch_data[1],
                      self.z: batch_z, self.is_training: True}
        return feed_dict        
    
    def batch_iterator(self, real_images, rep_batch, real_labels, **kwargs):
        missing_data = rep_batch - 1 - (real_images.shape[0] - 1) % rep_batch
        data         = real_images.reshape([-1] + self.image_shape)
        data         = np.vstack([data, np.zeros([missing_data] + self.image_shape)])
        labels       = self.one_hot(real_labels)
        labels       = np.vstack([labels, np.zeros((missing_data, self.n_labels))])
        paired_iter  = iter([data[i: i + rep_batch], labels[i: i + rep_batch]]
                                    for i in range(0, data.shape[0], rep_batch))
        return paired_iter
    
    def collect_data(self, list_out, shape):
        best_rep_imgs = super().collect_data(list_out, shape)
        best_rep_lab  = np.vstack([batch_out[1] for batch_out in list_out])
        best_rep_lab  = best_rep_lab[:shape[0]]
        return best_rep_imgs, best_rep_lab
            
    def reproduce(self, real_images, real_labels, **kwargs):
        replicas, labels = super().reproduce(real_images, real_labels=real_labels, **kwargs)
        return replicas, labels
    
    def create(self, n_images, labels, **kwargs):
        labels = self.one_hot(labels)
        return super().create(n_images, labels=labels, **kwargs)
    
    def discriminator_score(self, images, **kwargs):
        '''
        Return the discriminator score for the input images
        Arguments:
        - images: array of images;
        Returns:
        - scores: array of lenght len(images) containing the score of each image;
        - labels: 1D array with predicted classes [0, self.n_labels - 1].
        '''
        images = images.reshape([-1] + self.image_shape)
        scores, labels = self.sess.run([self.D, self.D_aux], feed_dict={self.images: images,
                                                                        self.is_training: False})
        return scores, labels
        
    def one_hot(self, labels):
        '''
        Encodes 1D vectors as 2D one-hot vector.
        Arguments:
        - labels: 1D array with values in [0, self.n_labels] or a 2D one-hot vector;
        Returns:
        - labels: 2D one-hot vector with shape (n_images, self.n_labels).
        '''
        labels = labels.astype(np.int)
        if labels.shape[-1] != self.n_labels:
                labels = np.eye(self.n_labels)[labels]
        return labels