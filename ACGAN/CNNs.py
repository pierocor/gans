import numpy as np
import tensorflow as tf
from GAN.ops import *
from GAN.CNNs import *
from tensorflow.keras.layers import UpSampling2D
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

class AC_Discriminator(Discriminator):
    def __init__(self, n_labels, **kwargs):
        super().__init__(**kwargs)
        
        self.aux_size = n_labels

        with tf.variable_scope(self.name) as scope:
            self.Waux = init_weights([self.len_linear, self.aux_size])
            self.baux = init_bias([self.aux_size])
            
    def __call__(self, X, training, reuse=False, **kwargs):
        D_score, logits_bias = super().__call__(X, training, reuse)
        
        with tf.variable_scope(self.scope_name) as scope:
            if reuse:
                scope.reuse_variables()
            #Fully Connected AC layer
            aux      = tf.matmul(self.flat, self.Waux)
            aux_bias = tf.nn.bias_add(aux, self.baux)
            logger.info('%slogits %s'% (self.name, str(np.shape(aux_bias))))

        return D_score, logits_bias, tf.math.softmax(aux_bias), aux_bias

class AC_Generator(Generator):
    def __init__(self, z_dim, n_labels, **kwargs):
        super().__init__(z_dim, **kwargs)

        with tf.variable_scope(self.scope_name):
            self.lab_emb = init_weights([n_labels, z_dim])

    def __call__(self, z, label, training, reuse=False):
        with tf.variable_scope(self.scope_name) as scope:
            if reuse:
                scope.reuse_variables()
        

        with tf.variable_scope('label_emb'):
            logger.info('%slabel %s'% (self.name, str(np.shape(label))))
            label_emb   = tf.matmul(label, self.lab_emb)
            _input = tf.multiply(z, label_emb)

        z4 = super().__call__(_input, training, reuse)
        return z4
