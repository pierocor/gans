import numpy as np
import tensorflow as tf
from GAN.ops import *
from GAN.CNNs import *
from tensorflow.keras.layers import UpSampling2D
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Cond_CNN(BaseCNN):
    def __init__(self, n_labels, emb_size, i_concat, **kwargs):
        '''
        Base class for Conditional CNNs. It can be used only
        inheriting from Discriminator or Generator.
        Arguments: n_labels, emb_size, i_concat, size_concat
        '''
        super().__init__(**kwargs)
        self.i_concat    = i_concat
        self.size_concat = self.compute_size(i_concat)
        
        assert i_concat < len(self.kernel_sizes)
        
        with tf.variable_scope(self.scope_name):
            W_shape = [self.kernel_sizes[i_concat], self.kernel_sizes[i_concat],
                       self.n_channels[i_concat]+1, self.n_channels[i_concat+1]]
            self.W[i_concat] = init_weights(W_shape)
            self.lab_emb = init_weights([n_labels, emb_size])
            self.lab_W   = init_weights([emb_size, self.size_concat**2])
            self.lab_b   = init_bias([1])
        
    
    def __call__(self, _tensor, label, training, reuse=False): 
        with tf.variable_scope(self.scope_name) as scope:
            if reuse:
                scope.reuse_variables()
            
            with tf.variable_scope('label_emb'):
                logger.info('%slabel %s'% (self.name, str(np.shape(label))))
                label_emb   = tf.matmul(label, self.lab_emb)
                label_layer = self.dense(label_emb, self.lab_W,
                                         self.lab_b, self.size_concat)
                           
            for i in range(len(self.n_channels)-1):
                if i == self.i_concat:
                    _tensor = tf.concat([_tensor, label_layer], axis=-1)
                    logger.info('%sconcat %s'% (self.name, str(np.shape(_tensor))))
                
                _tensor = self.conv_layer(i, _tensor)                
                
        return _tensor
    
class Cond_Discriminator(Cond_CNN, Discriminator):
    def __init__(self, n_labels, emb_size, i_concat, **kwargs):
        super().__init__(n_labels, emb_size, i_concat, **kwargs)
            
    def __call__(self, X, label, training, reuse=False):
        self.is_training = training
        logger.info('%sinput %s'% (self.name, str(np.shape(X))))
        
        conv_out = super().__call__(X, label, training, reuse)
        
        with tf.variable_scope(self.scope_name) as scope:
            if reuse:
                scope.reuse_variables()
            #Fully Connected output layer
            flat        = tf.reshape(conv_out, [-1, self.len_linear])
            logits      = tf.matmul(flat, self.W[4])
            logits_bias = tf.nn.bias_add(logits, self.b[4])
            logger.info('%slogits %s'% (self.name, str(np.shape(logits_bias))))

        return tf.math.sigmoid(logits_bias), logits_bias

class Cond_Generator(Cond_CNN, Generator):
    def __init__(self, z_dim, n_labels, emb_size, i_concat, **kwargs):
        '''
        Initialize Cond_Generator. 
        Arguments: z_dim, n_labels, emb_size, i_concat
        '''
        super().__init__(n_labels, emb_size, i_concat, z_dim=z_dim, **kwargs)
            
    def __call__(self, z, label, training, reuse=False):
        self.is_training = training
        logger.info('%sinput %s'% (self.name, str(np.shape(z))))
        
        
        with tf.variable_scope(self.scope_name) as scope:
            if reuse:
                scope.reuse_variables()
            
            #Fully Connected input layer
            conv_in = self.dense(z, self.W_lin, self.b_lin, self.small_size)
        
        #4 subsequent convolutional layers with concatenation
        img = super().__call__(conv_in, label, training, reuse)
        return img